export type ConsoleEffects = string | string[] | (() => any) | IterableIterator<string | string[] | (() => any)>;

interface ExecuteIteratorOptions {
  interceptHandler: (effect: Effects, handler: (effect: Effects) => any) => any;
}

const DEFAULT_OPTIONS: ExecuteIteratorOptions = {
  interceptHandler: (effect, handler) => handler(effect)
};

/**
 * Executes an ConsoleEffects iterator.
 * The effects are handled the following way:
 *  - string: print it to stdout
 *  - string[]: concat and print it to stdout
 *  - function: execute and await the result and push it back to the iterator
 */
export async function executeIterator(
  iterator: AsyncIterableIterator<Effects | ShorthandEffects> | IterableIterator<Effects | ShorthandEffects>,
  options = DEFAULT_OPTIONS
) {

  let nextArg: any;

  for (; ;) {
    const step = await iterator.next(nextArg);

    if (step.value !== undefined) {
      const effect = parseEffect(step.value);
      nextArg = await options.interceptHandler(effect, (e) => handleEffect(e, options));
    }

    if (step.done) {
      return nextArg;
    }
  }
}

async function handleEffect(effect: Effects, options: ExecuteIteratorOptions) {
  switch (effect.type) {
    case "CALL":
      return await effect.payload.fn();
    case "WRITE":
      process.stdout.write(effect.payload.buffer);
      return undefined;
    case "UNHANDLED":
      console.error("unhandled effect", effect.payload.effect);
      return undefined;
    case "BRANCH":
      return executeIterator(effect.payload.iterator, options);
  }
}

type Effects =
  CallEffect
  | BranchEffect
  | WriteEffect
  | UnhandledEffect;

interface CallEffect {
  readonly type: "CALL";
  payload: {
    fn: () => Promise<any>
  };
}

type ShorthandCallEffect = () => (Promise<any> | void);

interface BranchEffect {
  readonly type: "BRANCH";
  payload: {
    iterator: IterableIterator<Effects | ShorthandEffects>
  };
}

type ShorthandBranchEffect = IterableIterator<any>;

interface WriteEffect {
  readonly type: "WRITE";
  payload: {
    buffer: string
  };
}

type ShorthandWriteEffect = string | string[];

interface UnhandledEffect {
  readonly type: "UNHANDLED";
  payload: {
    effect: any
  };
}

type ShorthandEffects = ShorthandBranchEffect | ShorthandCallEffect | ShorthandWriteEffect;

type ConsoleIterator = IterableIterator<Effects>;

function parseEffect(value: any): Effects {

  const KNOWN_EFFECTS = ["CALL", "WRITE", "BRANCH"];

  if (value.type && KNOWN_EFFECTS.includes(value.type)) {
    return value;
  }

  if (typeof (value) === "function") {
    return {
      type: "CALL",
      payload: {
        fn: value
      }
    };
  }

  if (typeof (value) === "string") {
    return {
      type: "WRITE",
      payload: {
        buffer: value
      }
    };
  }

  if (isIterable(value)) {
    return {
      type: "BRANCH",
      payload: {
        iterator: value
      }
    };
  }

  return {
    type: "UNHANDLED",
    payload: {
      effect: value
    }
  };
}

export function isIterable(obj: any) {
  // checks for null and undefined
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === "function";
}

export function isAsyncIterable(obj: any) {
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.asyncIterator] === "function";
}
