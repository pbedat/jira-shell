import { Writable } from "stream";

/**
 * Wraps the standard output stream of the process (`process.stdout`)
 * and makes it mutable:
 * When the stream is muted any `write` calls will be skipped.
 * This is useful, e.g. when you are prompting for password inputs.
 */
export class MutableStdOut {

  public readonly stream = new Writable({
    write: (chunk, encoding, callback) => {
      if (!this.muted) {
        process.stdout.write(chunk, encoding);
      }
      callback();
    }
  });

  private muted = false;

  public mute() {
    this.muted = true;
  }

  public unmute() {
    this.muted = false;
  }
}
