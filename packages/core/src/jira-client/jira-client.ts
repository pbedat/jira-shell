import * as http from "http";
import * as https from "https";
import { compact, Dictionary, flatten, fromPairs, merge, range } from "lodash";
import fetch from "node-fetch";
import * as Url from "url";

import { iteratorToArray } from "../utils/iterator";
import {
  JiraField,
  JiraIssue,
  JiraIssueStatus,
  JiraResponse,
  JiraSearchResponse,
  JiraSprintsResponse,
  JiraWorklogResponse
} from "./types";

interface JiraClientContext {
  credentials: {
    username: string;
    password: string;
  };
  project: string;
  url: string;
  customFields: Record<string, string>;
}

let context: JiraClientContext;

export function configure(jiraClientContext: JiraClientContext) {
  context = {
    ...jiraClientContext,
    customFields: jiraClientContext.customFields || {}
  };
}

interface SearchOptions {
  pageSize?: number;
  expand: Array<"changelog">;
}

export interface SearchResponse {
  pageSize: number;
  total: number;
  iterator: AsyncIterableIterator<JiraIssue>;
  toPromise: () => Promise<JiraIssue[]>;
}

const DEFAULT_OPTIONS: SearchOptions = {
  pageSize: 50,
  expand: []
};

/**
 * search for jira issues using JQL
 * @param request
 */
export async function searchAsync(
  request: { jql: string },
  options: SearchOptions = DEFAULT_OPTIONS
): Promise<SearchResponse> {
  const { jql } = request;
  const pageSize: number =
    options.pageSize || (DEFAULT_OPTIONS.pageSize as number);

  let response = await execute<JiraSearchResponse>(
    // tslint:disable-next-line:max-line-length
    `/rest/api/2/search`,
    {
      maxResults: pageSize || DEFAULT_OPTIONS.pageSize,
      query: {
        jql,
        expand: (options.expand || []).join(",")
      }
    }
  );

  async function* iterate(
    jiraResponse: JiraSearchResponse & JiraResponse
  ): AsyncIterableIterator<JiraIssue> {
    for (const issue of jiraResponse.issues) {
      const { fields, changelog } = issue;
      // parse rank custom field
      fields.rank = fields[
        context.customFields.rang || context.customFields.rank
      ] as string;

      // parse sprint custom field
      if (fields[(context.customFields || ({} as any)).sprint]) {
        fields.sprints = (fields[
          (context.customFields || ({} as any)).sprint
        ] as any).map(parseSprintField);
      }

      if (changelog && changelog.total > changelog.maxResults) {
        console.log(
          `skipped changelog: total '${changelog.total}' loaded '${
            changelog.maxResults
          }'`
        );
      }

      yield issue;
    }

    if (jiraResponse.startAt + pageSize < response.total) {
      response = await execute<JiraSearchResponse>(
        // tslint:disable-next-line:max-line-length
        `/rest/api/2/search`,
        {
          startAt: jiraResponse.startAt + pageSize,
          maxResults: pageSize,
          query: {
            jql,
            expand: (options.expand || []).join(",")
          }
        }
      );

      yield* iterate(response);
    }
  }

  return {
    pageSize,
    total: response.total,
    iterator: iterate(response),
    toPromise: () => iteratorToArray(iterate(response))
  };
}

export async function search(
  request: { jql: string },
  options: SearchOptions = { expand: [] }
) {
  const response = await searchAsync(request, options);

  return await response.toPromise();
}

export async function getWorklog({ key }: JiraIssue) {
  const PAGE_SIZE = 50;

  const worklog = await execute<JiraWorklogResponse>(
    `/rest/api/2/issue/${key}/worklog`,
    { maxResults: PAGE_SIZE }
  );

  const responses = await Promise.all(
    range(worklog.worklogs.length, worklog.total, PAGE_SIZE).map(startAt =>
      execute<JiraWorklogResponse>(`/rest/api/2/issue/${key}/worklog`, {
        maxResults: PAGE_SIZE,
        startAt
      })
    )
  );

  return flatten([worklog, ...responses].map(({ worklogs }) => worklogs));
}

function parseSprintField(field: string): any {
  const matches = field.match(/\[([^\]]+)]/);

  if (matches === null) {
    return null;
  }

  const sprint = fromPairs(
    matches[1].split(",").map(token => token.split("="))
  );

  sprint.id = parseInt(sprint.sequence, 0);

  return sprint;
}

export async function getAllBoards() {
  return execute<JiraBoardsResponse>(`/rest/agile/1.0/board`, {
    query: { projectKeyOrId: context.project }
  });
}

export function getFields() {
  return execute<JiraField[]>("/rest/api/2/field");
}

interface JiraBoardsResponse {
  values: Array<{
    id: number;
    self: string;
    name: string;
    type: "scrum" | "kanban";
  }>;
}

export async function getAllSprints() {
  const { values } = await getAllBoards();

  const boardSprints = await Promise.all(
    values.map(async b => {
      try {
        return await getSprints({ boardId: b.id });
      } catch (err) {
        // TODO add logging here:
        // commented this out because of the --quiet option
        // process.stderr.write(`unable to fetch sprints for board ${b.id}\n`);
        return ([] as any) as (JiraResponse & JiraSprintsResponse);
      }
    })
  );

  return compact(flatten(boardSprints.map(bs => bs.values)));
}

export async function getSprints(request: { boardId: number }) {
  return execute<JiraSprintsResponse>(
    `/rest/agile/1.0/board/${request.boardId}/sprint`
  );
}

export async function getStatusList() {
  return execute<JiraIssueStatus[]>(`/rest/api/latest/status`);
}

async function execute<T = {}>(
  path: string,
  options: {
    query?: { [key: string]: any };
    maxResults?: number;
    startAt?: number;
  } = {}
): Promise<JiraResponse & T> {
  const { credentials, url: baseUrl } = context;
  const authorization = `Basic ${Buffer.from(
    `${credentials.username}:${credentials.password}`
  ).toString("base64")}`;

  // const url = Url.parse(baseUrl + path);
  const url = new Url.URL(baseUrl + path);

  if (options.maxResults) {
    url.searchParams.append("maxResults", options.maxResults.toString());
  }
  if (options.startAt) {
    url.searchParams.append("startAt", options.startAt.toString());
  }

  if (options.query) {
    // tslint:disable-next-line: forin
    for (const queryParam in options.query) {
      const value = options.query[queryParam];
      if (value) {
        url.searchParams.append(queryParam, value.toString());
      }
    }
  }

  try {
    const response = await fetch(url.toString(), {
      headers: {
        Authorization: authorization
      },
      method: "GET"
    });

    if (!response.ok) {
      return Promise.reject(
        "failed to execute " +
          url +
          " " +
          response.status +
          " " +
          response.statusText
      );
    }

    const jiraResponse = (await response.json()) as JiraResponse & T;

    while (jiraResponse.isLast === false && !options.maxResults) {
      merge(
        jiraResponse,
        await execute(path, {
          startAt: jiraResponse.startAt + jiraResponse.maxResults,
          maxResults: options.maxResults || jiraResponse.maxResults,
          query: options.query
        })
      );
    }

    return jiraResponse;
  } catch (err) {
    console.log("failed to execute ", path, credentials);
    return Promise.reject(err);
  }
}
