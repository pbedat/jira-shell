import { first, isString, last, memoize, orderBy, uniq } from "lodash";
import { JiraSprint } from "./types";

export type SprintNameArg = string | undefined | "__current" | "__next";

/**
 * Retrieves the sprint from a list of sprint based on the provided sprint name
 *
 * When no sprintName is provided
 *  - the next upcoming sprint will be selected
 *  - the current sprint will be selected if no upcomint sprint is planned
 *
 * The sprint name can also be defined by special variables:
 *  - %current%: the currently running sprint
 *  - %next% the next upcoming sprint
 */
export function getSprint(sprintName: SprintNameArg, sprints: JiraSprint[]) {
  if (!sprintName || !isString(sprintName)) {
    return getNextSprint(sprints) || getActiveSprint(sprints);
  }

  switch (sprintName.toUpperCase()) {
    case "__CURRENT":
      return getActiveSprint(sprints);
    case "__NEXT":
      return getNextSprint(sprints);
  }

  return sprints.find(({ name }) => name === sprintName);
}

const getNextSprint = memoize((sprints: JiraSprint[]) => {
  return first(
    orderBy(
      sprints.filter(s => !s.completeDate && s.state !== "active"),
      (s: JiraSprint) => s.startDate || s.name
    )
  );
});

const getActiveSprint = memoize((sprints: JiraSprint[]) => {
  console.log(uniq(sprints.map(s => s.state)));
  return last(
    orderBy(
      sprints.filter(s => !s.completeDate && s.state === "active"),
      (s: JiraSprint) => s.completeDate || s.name
    )
  );
});
