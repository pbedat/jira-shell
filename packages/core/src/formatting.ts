import json5 = require("json5");
import { identity, isString, repeat } from "lodash";
import { table } from "table";

/**
 * Formats seconds as '<hours> h'
 * @param seconds
 */
export function formatHours(seconds: number) {
  return seconds ? `${Math.round((seconds / 3600) * 100) / 100} h` : "-";
}

/**
 * Takes a string and puts some lines under it:
 * 'foo' ==> 'foo\n---'
 * @param str
 */
export function underline(str: string, pattern = "-") {
  return [str, repeat(pattern, str.length)].join("\n");
}

export enum TableFormat {
  csv = "csv",
  ascii = "ascii"
}

interface ColumnDefinitionOptions {
  label: string;
  valueFormatter?: (val: any, format: TableFormat) => string;
}

interface ColumnDefinition extends ColumnDefinitionOptions {
  valueFormatter: (val: any, format: TableFormat) => string;
}

// TODO: createDataTable
function createTableDefinition<T>(
  columns: Array<[keyof T, string | ColumnDefinitionOptions]>
) {
  return new Map<keyof T, ColumnDefinitionOptions>(columns.map(
    ([field, definition]) => {
      return [
        field as string,
        isString(definition)
          ? ({ label: definition, valueFormatter: identity } as ColumnDefinition)
          : ({ valueFormatter: identity, ...definition } as ColumnDefinition)
      ];
    }
  ) as Array<[keyof T, ColumnDefinitionOptions]>);
}

interface FormatArgs<T> {
  data: T[];
  columns: Array<[keyof T, string | ColumnDefinitionOptions]>;
  format: TableFormat;
}

/**
 * Formats an array of objects as a tabular string representation based on the provided `format`
 * @param items An array of data, where the array items are used as rows and the properties as columns
 * @param columns defines the order and the names of the table columns
 * @param format the output format: csv, ascii
 */
export function formatDataTable<T>({columns, format, data}: FormatArgs<T>) {
  const columnDefinition = createTableDefinition<T>(columns);
  // renders the `dataTable` as a two dimensional string array,
  // where the first dimension holds the rows and the second dimension the columns
  const renderedTable = [
    // add the column headers
    [...columnDefinition.values()].map(cd => cd.label),
    // add the rows
    ...data.map(row =>
      [...columnDefinition.keys()].map(col =>
        (columnDefinition.get(col) as ColumnDefinition).valueFormatter(
          row[col],
          format
        )
      )
    )
  ];

  switch (format) {
    case "csv":
      return renderedTable.map(row => row.join(";")).join("\n");
    case "ascii":
      return table(renderedTable);
    default:
      throw new Error(`unknown format ${format}`);
  }
}
