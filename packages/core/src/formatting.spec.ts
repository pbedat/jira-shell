import { format, parse } from "date-fns";
import { formatDataTable, formatHours, TableFormat } from "./formatting";

// This is an example test... don't take it to serious
describe("formatter formatHours", () => {
  describe("formatHours", () => {
    it("should get the format right", () => {
      const testData = [
        // 3600 seconds to "1 h"
        { seconds: 3600, result: "1 h" },
        // 7200 seconds to "2 h"
        { seconds: 7200, result: "2 h" },
        // 1800 seconds to "0.5 h"
        { seconds: 1800, result: "0.5 h" },
        // -3600 seconds to "-1 h"
        { seconds: -3600, result: "-1 h" }
      ];
      testData.forEach(({ seconds, result }) => {
        const actual = formatHours(seconds);
        expect(actual).toEqual(result);
      });
    });
  });
});

describe("table formatting", () => {
  describe("format csv", () => {
    const data = [
      { name: "foo", dateOfBirth: new Date("1989-05-10") },
      { name: "bar", dateOfBirth: new Date("1986-03-29") }
    ];

    const csv = formatDataTable({
      data,
      columns: [
        [
          "dateOfBirth",
          {
            label: "Birthday",
            valueFormatter: (date: Date) => format(date, "dd.MM.yyyy")
          }
        ],
        ["name", "Name"]
      ],
      format: TableFormat.csv
    });

    it("should format the fields in the defined order with the correct labels", () => {
      const header = csv.split("\n")[0];
      expect(header).toBe("Birthday;Name");
    });

    it("should have 'identity' as cell formatter default", () => {
      const nameColumn = csv
        .split("\n")
        .slice(1)
        .map(row => row.split(";")[1]);
      expect(nameColumn).toEqual(["foo", "bar"]);
    });

    it("should format the values based on the formatter", () => {
      const birthdayColumn = csv
        .split("\n")
        .slice(1)
        .map(row => row.split(";")[0]);
      expect(birthdayColumn).toEqual(["10.05.1989", "29.03.1986"]);
    });
  });
});
