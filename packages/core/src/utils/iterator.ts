export async function iteratorToArray<T>(iterator: IterableIterator<T> | AsyncIterableIterator<T>) {
  const items = [];

  for await (const item of iterator) {
    items.push(item);
  }

  return items;
}
