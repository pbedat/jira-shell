import { limitWorkload } from "./issues";
import { JiraIssue, JiraIssueFields } from "./jira-client";

describe("limitWorkload", () => {

  // id, timeestimate, rank
  const issues = [
    [ "TEST-1", 2 * 3600, "3" ],
    [ "TEST-2", 4 * 3600, "2" ],
    [ "TEST-3", 6 * 3600, "1" ]
  ].map(toJiraIssue);

  it("should limit the workload exactly", () => {
    const expected = issues.slice(0, 2);
    const actual = limitWorkload(issues, 6 * 3600);
    expect(actual).toEqual(expected);
  });

  it("should allow the workload to overlap", () => {
    const expected = issues.slice(0, 2);
    const actual = limitWorkload(issues, 5 * 3600);
    expect(actual).toEqual(expected);
  });

  it("apply the limitation in the correct order", () => {
    const expected = issues.reverse().slice(0, 2);
    const actual = limitWorkload(issues, 8 * 3600, { orderBy: "rank"});
    expect(actual).toEqual(expected);
  });
});

function toJiraIssue(tuple: any[]) {
  const [id, timeestimate, rank] = tuple;
  return {
    id,
    fields: {
      timeestimate,
      rank
    }
  } as JiraIssue;
}
