export async function* map<T, U>(iterator: Iterable<T>, mapFn: (arg: T) => U) {
  for await (const item of iterator) {
    yield mapFn(item);
  }
}

export function* chunk<T>(iterator: IterableIterator<T>, size: number) {

  let buffer = take(iterator, size);

  while (buffer.length) {
    yield buffer;
    buffer = take(iterator, size);
  }
}

export function* flatten<T>(iterator: IterableIterator<T[]>) {
  for (const array of iterator) {
    for (const item of array) {
      yield item;
    }
  }
}

export async function* flattenIterable<T>(iterator: AsyncIterableIterator<AsyncIterableIterator<T>>) {
  for await(const asyncIterator of iterator) {
    yield* asyncIterator;
  }
}

function take<T>(iterator: IterableIterator<T>, count: number) {

  const list = [];

  for (let i = 0; i < count; i++) {
    const result = iterator.next();

    if (result.done) {
      break;
    }

    list.push(result.value);
  }

  return list;
}
