# `core`

Library for working with the jira rest api.
Currently only used by the `jira-shell`.

# Features

- Types for the JIRA rest api
- A simple incomplete api client
- tools and utils used by the `jira-shell`