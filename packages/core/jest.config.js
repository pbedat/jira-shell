module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ["/node_modues/", "/dist/"],
  globals: {
    __TEST__: true
  },
  moduleNameMapper: {
  }
};