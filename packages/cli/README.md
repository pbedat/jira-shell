The jira-shell is a swiss army knife of tooling around Jira.

Our primary goal is to work around the shortcomings of Jira Software, imposed by
the very rigid management corsets in the enterprise projects we work in.

This program is meant for software engineers: Most of the commands of the application
run in the context of a single Jira user. E.g. listing issues in preparation of a daily standup
gives you only the tickets to you.

Since software engineers are seldom just hacking stuff together, we also provide commands for
planning or reporting. Because we are 'unfortunately' involved in those activities ;) 

# Installation

Prerequisites: https://nodejs.org/en/download/

If you just want to give it a try or you work in an environment where don't have elevated rights,
you can run the jira-shell directly:

```
npx jira-shell --help
```

The more convenient way is to install it globally:

```
# with elevated rights
npm install -g jira-shell
```

# Getting Started

While usable without any preparations, we absolutely recommend to initialize
a jira-shell project in the root of the repository you are working with.

The project file contains all the static context of your project and saves you
from typing in a lot of parameters like:
 - Jira URL
 - Project Key
 - Your username
 
```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "init" "--help"

src init

initializes a jira-shell project

Options:
  --version           Show version number                              [boolean]
  --username, -u, -u
  --password, -p      Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                      base64 encoding
  --hyperlinks, -l    experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help              Show help                                        [boolean]
  --url               URL to a JIRA instance
  --project, -p

```

# Help

The `help` command is available almost everywhere: in the root level and at every command level.

```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "--help"

src [command]

Commands:
  src list <list>           lists jira issues                      [aliases: ls]
  src workload [--sprint]   calculates the workload per assignee
  src report <report>       Show aggregated reporting data over a list of jira
                            issues
  src report:actual-target  Compares the history of the estimates of initial
                            tickets and added tickets against the actual work
                            logged
  src timesheet             prints your logged work as a timesheet
  src init                  initializes a jira-shell project

Options:
  --version         Show version number                                [boolean]
  --username, -u    Can be preset with env var JIRA_SHELL_USERNAME
  --password, -p    Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                    base64 encoding
  --hyperlinks, -l  experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help            Show help                                          [boolean]

```

# Commands

## list (ls)

With the `list` command you can get quick overviews, that don't need any further arguments.

```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "ls" "--help"

src list <list>

lists jira issues

Positionals:
  list                   [required] [choices: "in-progress", "sprints", "daily"]

Options:
  --version         Show version number                                [boolean]
  --username, -u    Can be preset with env var JIRA_SHELL_USERNAME
  --password, -p    Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                    base64 encoding
  --hyperlinks, -l  experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help            Show help                                          [boolean]
  --url             base url of the Jira instance

```


The only required argument argument is the positional `list` argument.
The following lists are available:
 - in-progress: lists all issues, that are in progress in the current sprint
 - sprints: lists all sprints
 - daily: lists issues grouped by status, useful for daily meetings

Examples:

**in-progress**

```
Resolved
------------------
ICLX-12345 UC00.20W: Spalten in einem Datengrid filtern ([https://my-jira/browse/ICLX-12345](Open in Jira))

In Progress
---------------------
ICLX-12346 UC00.20W: Spalten in einem Datengrid sortieren ([https://my-jira/browse/ICLX-12346](Open in Jira))

Open (next ~8h)
-------------------------
>> no issues 'Open (next ~8h)'
>> 4 issues to go: 31h
```

*Resolved* issues shows issues resolved since the last workday and ordered by their resolution date.
*In Progress* and *Open* issues are ordered by their rank.
*Open* only shows upcoming issues with a remaining estimate ~8h. E.g. if the next issues are estimated with 4, 5 and 8 hours - only the first two would be listed.

## workload

Shows the workload of assignees in a sprint. This is helpful, when planning an upcoming sprint and assigning tickets. 
Or to check if issues should be reassigned in a running sprint.

**Example:**

```
> jira-shell workload

Workload for sprint >>2018-11-27<<
╔═════════════════╤════════╤══════════╗
║ Assignee        │ Issues │ Workload ║
╟─────────────────┼────────┼──────────╢
║ Alexey          │      4 │     21 h ║
╟─────────────────┼────────┼──────────╢
║ Andreas         │      3 │     42 h ║
╟─────────────────┼────────┼──────────╢
║ Armin           │      4 │        - ║
╟─────────────────┼────────┼──────────╢
║ Elisabeth       │      1 │        - ║
╟─────────────────┼────────┼──────────╢
║ Jens            │      1 │        - ║
╟─────────────────┼────────┼──────────╢
║ Patrick         │      7 │     32 h ║
╟─────────────────┼────────┼──────────╢
║ Rene            │      3 │     20 h ║
╟─────────────────┼────────┼──────────╢
║ Unassigned      │     76 │    639 h ║
╟─────────────────┼────────┼──────────╢
║ Total           │     99 │    754 h ║
╚═════════════════╧════════╧══════════╝
```

```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "workload" "--help"

src workload [--sprint]

calculates the workload per assignee

Options:
  --version         Show version number                                [boolean]
  --username, -u    Can be preset with env var JIRA_SHELL_USERNAME
  --password, -p    Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                    base64 encoding
  --hyperlinks, -l  experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help            Show help                                          [boolean]
  --url             base url of the Jira instance
  --sprint          Calculates the workload explicitly for this sprint.
                    When no sprint is provided the workload will be calculated
                    for:
                    - the next upcoming sprint (Default)
                    - the currently running sprint, when no upcoming sprint is
                    planned
                    Both, upcoming and currently running sprints, are available
                    with funcion variables:
                    - __current: the current sprint
                    - __next: the next upcomping sprint

  --quiet, -q       print only issue keys
  --groupBy         Groups Issues by the provided issue field

```

## report:scope

The report:scope command prints all changes of the sprint over the course of the sprint.

```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "report:scope" "--help"

src report:scope

Options:
  --version         Show version number                                [boolean]
  --username, -u    Can be preset with env var JIRA_SHELL_USERNAME
  --password, -p    Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                    base64 encoding
  --hyperlinks, -l  experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help            Show help                                          [boolean]
  --url             base url of the Jira instance
  --sprint          a sprint name or a variable: __current or __next
  --format          defines the output format: ascii, csv, json
                            [choices: "json", "csv", "ascii"] [default: "ascii"]

```

Example:

```
╔════════════╤═══════╤═════════╤════════════╤═════════╤═════════╗
║ Date       │ Added │ Removed │ ∑ Original │ ∑ Added │ ∑ Total ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 08.11.2018 │ 6     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 09.11.2018 │ 0     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 10.11.2018 │ 0     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 11.11.2018 │ 0     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 12.11.2018 │ 1     │ 0       │ 101        │ 7       │ 108     ║
╚════════════╧═══════╧═════════╧════════════╧═════════╧═════════╝
```

## report:actual-target

The report:actual-target command gives you an comparison of estimates vs. workloads.

```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "report:actual-target" "--help"

src report:actual-target

Compares the history of the estimates of initial tickets and added tickets
against the actual work logged

Options:
  --version         Show version number                                [boolean]
  --username, -u    Can be preset with env var JIRA_SHELL_USERNAME
  --password, -p    Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                    base64 encoding
  --hyperlinks, -l  experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help            Show help                                          [boolean]
  --url             base url of the Jira instance
  --sprint          a sprint name or a variable: __current or __next
  --format          defines the output format: ascii, csv, json
                            [choices: "json", "csv", "ascii"] [default: "ascii"]

```

Example:

```
╔════════════╤═══════════════════╤════════════════╤══════════════════╤═══════════════╗
║ Date       │ Original Estimate │ Added Estimate │ Original Worklog │ Added Worklog ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 30.11.2018 │ 337.75 h          │ 32.75 h        │ 17.25 h          │ -             ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 01.12.2018 │ 337.75 h          │ 32.75 h        │ 17.25 h          │ -             ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 02.12.2018 │ 337.75 h          │ 32.75 h        │ 17.25 h          │ -             ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 03.12.2018 │ 305.25 h          │ 8 h            │ 56.25 h          │ 5 h           ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 04.12.2018 │ 299.5 h           │ 1 h            │ 77 h             │ 5 h           ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 05.12.2018 │ 253.75 h          │ 1 h            │ 128.25 h         │ 5 h           ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 06.12.2018 │ 222 h             │ 1 h            │ 168.75 h         │ 5 h           ║
╚════════════╧═══════════════════╧════════════════╧══════════════════╧═══════════════╝
```

## timesheet

Prints a timesheet of your worklog for a given date (default: today);

```

> jira-shell@1.0.2-alpha.1 start C:\Users\pbedat\projects\jira-shell\packages\cli
> node ./node_modules/ts-node/dist/bin.js -r tsconfig-paths/register src/ "timesheet" "--help"

src timesheet

prints your logged work as a timesheet

Options:
  --version         Show version number                                [boolean]
  --username, -u    Can be preset with env var JIRA_SHELL_USERNAME
  --password, -p    Can be preset with env var JIRA_SHELL_PASSWORD_BASE64 in
                    base64 encoding
  --hyperlinks, -l  experimental cli-hyperlinks integration
                                                      [boolean] [default: false]
  --help            Show help                                          [boolean]
  --url             base url of the Jira instance
  --date                                   [default: "2019-01-04T12:02:09.377Z"]

```

Example:

```
$ ts-node src/ timesheet --date 2018-11-08

Worklog 08.11.18
================

5.25h / 8h [#####################___________]

max length
max length
╔═════════╤══════════╤════════════╤══════════════════════════════════════════════════════════════════╤═══════════════════════════════════════════════════════╗
║ 00:00   │ 15m      │ ICLX-11111 │ Grobe Spezifikation des Authorisierungs Endpunkts                │ scharf nachgedacht                                    ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ (00:15) │ (0.25 h) │ ---------- │ ---------------------------------------------------------------- │ ¯\_(ツ)_/¯                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 00:30   │ 2h       │ ICLX-11112 │ Zeilen in einem Datengrid selektieren                            │ Konzeption                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 02:30   │ 2h 30m   │ ICLX-11113 │ Meetings                                                         │ Meeting Vorbereitet                                   ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 05:00   │ 15m      │ ICLX-11111 │ Grobe Spezifikation des Authorisierungs Endpunkts                │ Kommentare                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ (05:15) │ (9.15 h) │ ---------- │ ---------------------------------------------------------------- │ ¯\_(ツ)_/¯                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 14:24   │ 15m      │ ICLX-1114  │ Filterung und Sortierung im Grid ermöglichen                     │                                                       ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ (14:39) │ (2.75 h) │ ---------- │ ---------------------------------------------------------------- │ ¯\_(ツ)_/¯                                            ║
╚═════════╧══════════╧════════════╧══════════════════════════════════════════════════════════════════╧═══════════════════════════════════════════════════════╝
```