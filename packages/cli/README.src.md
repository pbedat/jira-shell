The jira-shell is a swiss army knife of tooling around Jira.

Our primary goal is to work around the shortcomings of Jira Software, imposed by
the very rigid management corsets in the enterprise projects we work in.

This program is meant for software engineers: Most of the commands of the application
run in the context of a single Jira user. E.g. listing issues in preparation of a daily standup
gives you only the tickets to you.

Since software engineers are seldom just hacking stuff together, we also provide commands for
planning or reporting. Because we are 'unfortunately' involved in those activities ;) 

# Installation

Prerequisites: https://nodejs.org/en/download/

If you just want to give it a try or you work in an environment where don't have elevated rights,
you can run the jira-shell directly:

```
npx jira-shell --help
```

The more convenient way is to install it globally:

```
# with elevated rights
npm install -g jira-shell
```

# Getting Started

While usable without any preparations, we absolutely recommend to initialize
a jira-shell project in the root of the repository you are working with.

The project file contains all the static context of your project and saves you
from typing in a lot of parameters like:
 - Jira URL
 - Project Key
 - Your username
 
$(npm start -- init --help)

# Help

The `help` command is available almost everywhere: in the root level and at every command level.

$(npm start -- --help)

# Commands

## list (ls)

With the `list` command you can get quick overviews, that don't need any further arguments.

$(npm start -- ls --help)


The only required argument argument is the positional `list` argument.
The following lists are available:
 - in-progress: lists all issues, that are in progress in the current sprint
 - sprints: lists all sprints
 - daily: lists issues grouped by status, useful for daily meetings

Examples:

**in-progress**

```
Resolved
------------------
ICLX-12345 UC00.20W: Spalten in einem Datengrid filtern ([https://my-jira/browse/ICLX-12345](Open in Jira))

In Progress
---------------------
ICLX-12346 UC00.20W: Spalten in einem Datengrid sortieren ([https://my-jira/browse/ICLX-12346](Open in Jira))

Open (next ~8h)
-------------------------
>> no issues 'Open (next ~8h)'
>> 4 issues to go: 31h
```

*Resolved* issues shows issues resolved since the last workday and ordered by their resolution date.
*In Progress* and *Open* issues are ordered by their rank.
*Open* only shows upcoming issues with a remaining estimate ~8h. E.g. if the next issues are estimated with 4, 5 and 8 hours - only the first two would be listed.

## workload

Shows the workload of assignees in a sprint. This is helpful, when planning an upcoming sprint and assigning tickets. 
Or to check if issues should be reassigned in a running sprint.

**Example:**

```
> jira-shell workload

Workload for sprint >>2018-11-27<<
╔═════════════════╤════════╤══════════╗
║ Assignee        │ Issues │ Workload ║
╟─────────────────┼────────┼──────────╢
║ Alexey          │      4 │     21 h ║
╟─────────────────┼────────┼──────────╢
║ Andreas         │      3 │     42 h ║
╟─────────────────┼────────┼──────────╢
║ Armin           │      4 │        - ║
╟─────────────────┼────────┼──────────╢
║ Elisabeth       │      1 │        - ║
╟─────────────────┼────────┼──────────╢
║ Jens            │      1 │        - ║
╟─────────────────┼────────┼──────────╢
║ Patrick         │      7 │     32 h ║
╟─────────────────┼────────┼──────────╢
║ Rene            │      3 │     20 h ║
╟─────────────────┼────────┼──────────╢
║ Unassigned      │     76 │    639 h ║
╟─────────────────┼────────┼──────────╢
║ Total           │     99 │    754 h ║
╚═════════════════╧════════╧══════════╝
```

$(npm start -- workload --help)

## report:scope

The report:scope command prints all changes of the sprint over the course of the sprint.

$(npm start -- report:scope --help)

Example:

```
╔════════════╤═══════╤═════════╤════════════╤═════════╤═════════╗
║ Date       │ Added │ Removed │ ∑ Original │ ∑ Added │ ∑ Total ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 08.11.2018 │ 6     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 09.11.2018 │ 0     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 10.11.2018 │ 0     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 11.11.2018 │ 0     │ 0       │ 101        │ 6       │ 107     ║
╟────────────┼───────┼─────────┼────────────┼─────────┼─────────╢
║ 12.11.2018 │ 1     │ 0       │ 101        │ 7       │ 108     ║
╚════════════╧═══════╧═════════╧════════════╧═════════╧═════════╝
```

## report:actual-target

The report:actual-target command gives you an comparison of estimates vs. workloads.

$(npm start -- report:actual-target --help)

Example:

```
╔════════════╤═══════════════════╤════════════════╤══════════════════╤═══════════════╗
║ Date       │ Original Estimate │ Added Estimate │ Original Worklog │ Added Worklog ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 30.11.2018 │ 337.75 h          │ 32.75 h        │ 17.25 h          │ -             ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 01.12.2018 │ 337.75 h          │ 32.75 h        │ 17.25 h          │ -             ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 02.12.2018 │ 337.75 h          │ 32.75 h        │ 17.25 h          │ -             ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 03.12.2018 │ 305.25 h          │ 8 h            │ 56.25 h          │ 5 h           ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 04.12.2018 │ 299.5 h           │ 1 h            │ 77 h             │ 5 h           ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 05.12.2018 │ 253.75 h          │ 1 h            │ 128.25 h         │ 5 h           ║
╟────────────┼───────────────────┼────────────────┼──────────────────┼───────────────╢
║ 06.12.2018 │ 222 h             │ 1 h            │ 168.75 h         │ 5 h           ║
╚════════════╧═══════════════════╧════════════════╧══════════════════╧═══════════════╝
```

## timesheet

Prints a timesheet of your worklog for a given date (default: today);

$(npm start -- timesheet --help)

Example:

```
$ ts-node src/ timesheet --date 2018-11-08

Worklog 08.11.18
================

5.25h / 8h [#####################___________]

max length
max length
╔═════════╤══════════╤════════════╤══════════════════════════════════════════════════════════════════╤═══════════════════════════════════════════════════════╗
║ 00:00   │ 15m      │ ICLX-11111 │ Grobe Spezifikation des Authorisierungs Endpunkts                │ scharf nachgedacht                                    ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ (00:15) │ (0.25 h) │ ---------- │ ---------------------------------------------------------------- │ ¯\_(ツ)_/¯                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 00:30   │ 2h       │ ICLX-11112 │ Zeilen in einem Datengrid selektieren                            │ Konzeption                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 02:30   │ 2h 30m   │ ICLX-11113 │ Meetings                                                         │ Meeting Vorbereitet                                   ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 05:00   │ 15m      │ ICLX-11111 │ Grobe Spezifikation des Authorisierungs Endpunkts                │ Kommentare                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ (05:15) │ (9.15 h) │ ---------- │ ---------------------------------------------------------------- │ ¯\_(ツ)_/¯                                            ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ 14:24   │ 15m      │ ICLX-1114  │ Filterung und Sortierung im Grid ermöglichen                     │                                                       ║
╟─────────┼──────────┼────────────┼──────────────────────────────────────────────────────────────────┼───────────────────────────────────────────────────────╢
║ (14:39) │ (2.75 h) │ ---------- │ ---------------------------------------------------------------- │ ¯\_(ツ)_/¯                                            ║
╚═════════╧══════════╧════════════╧══════════════════════════════════════════════════════════════════╧═══════════════════════════════════════════════════════╝
```