module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ["/node_modues/", "/dist/"],
  globals: {
    __TEST__: true
  },
  moduleNameMapper: {
    "^@jira-shell/core/(.*)$": "<rootDir>/../core/src/$1"
  }
};