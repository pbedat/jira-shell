import {format} from "date-fns";
import { cursorTo } from "readline";

import { JiraIssue, JiraSprint, searchAsync, SearchResponse } from "@jira-shell/core/jira-client";

const DEFAULT_OPTIONS = {
  includeSubTasks: false
};

/**
 * Loads issues that are currently on a sprint or have been on the sprint since it started
 * @param sprint
 */

export async function* loadIssuesRelatedToSprint(sprint: JiraSprint, options = DEFAULT_OPTIONS) {

  // to measure the duration of the load process
  const start = new Date();

  // find all issues, that are
  // - either currently on the sprint
  // - have changes between the sprint start and end
  // those issues with changes might be issues, that have been moved away from the sprint,
  // while the sprint was active
  const relevantIssues: SearchResponse = yield () => searchAsync(
    {
      jql: [
        options.includeSubTasks ? "" : `issuetype != Sub-Task AND`,
        `(sprint = ${sprint.id}`,
        `OR (updatedDate > "${format(sprint.startDate, "yyyy-MM-dd HH:mm")}"`,
        `AND updatedDate < "${format(sprint.endDate, "yyyy-MM-dd HH:mm")}"))`
        // "AND issuetype not in (Structureelement, Test, ReqEpic, ReqUserStory)"
      ].join(" ")
    },
    { expand: ["changelog"], pageSize: 50 }
  );

  // this will be the result
  const issues: JiraIssue[] = [];

  process.stdout.write(`Loading... 0%`);

  for await (const issue of relevantIssues.iterator) {
    issues.push(issue);
    cursorTo(process.stdout, 0);
    process.stdout.write(`Loading... ${(issues.length / relevantIssues.total * 100).toFixed(2)}%`);
  }

  yield `\n${new Date().getTime() - start.getTime()}ms\n\n`;

  return issues;
}
