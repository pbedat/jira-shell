import { config } from "dotenv";

config();

import { main } from "./main";

// tslint:disable-next-line:no-string-literal
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0 as any;

main(process.argv);
