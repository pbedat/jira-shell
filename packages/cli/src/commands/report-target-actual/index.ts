import { format } from "date-fns";

import { formatDataTable, TableFormat } from "@jira-shell/core/formatting";
import {
  getAllSprints,
  getSprint,
  JiraIssue
} from "@jira-shell/core/jira-client";
import { loadIssuesRelatedToSprint } from "../../queries/load-issues-related-to-sprint";
import { WithContextCommandModule } from "../../shared/with-context";
import { ActualTargetReportRow, calculateActualTargetReport } from "./report";

interface Args {
  sprint?: string;
  format: TableFormat | "json";
}

export const reportActualTargetModule: WithContextCommandModule = {
  builder: y =>
    y
      .option("sprint", {
        description: "a sprint name or a variable: __current or __next"
      })
      .option("format", {
        description: "defines the output format: ascii, csv, json",
        choices: ["json", "csv", "ascii"],
        default: "ascii"
      }),
  describe: `Compares the history of the estimates of initial tickets and added tickets against the actual work logged`,
  command: "report:actual-target",
  async *handler(request: { args: Args }) {
    // find the sprint for which we want to create the report
    const sprints = yield () => getAllSprints();
    const sprint = getSprint(request.args.sprint || "__current", sprints);

    if (!sprint) {
      yield `sprint ${request.args.sprint} not found\n`;
      return;
    }

    // load issues related to the sprint
    const issues: JiraIssue[] = yield* loadIssuesRelatedToSprint(sprint);

    // calculate the report
    const report = calculateActualTargetReport(sprint, issues);

    // print the report in the requested format
    yield print([...report], request.args.format);
  }
};

/**
 * Prints the report in the given tabular string format.
 *
 * The following columns will be printed:
 *  - Date
 *  - Original Estimate
 *  - Added Estimate
 *  - Original Worklog
 *  - Added Worklog
 */
function print(
  report: ActualTargetReportRow[],
  tableFormat: TableFormat | "json"
) {
  if (tableFormat === "json") {
    return JSON.stringify([...report]);
  }

  return formatDataTable<ActualTargetReportRow>({
    data: report,
    columns: [
      [
        "date",
        {
          label: "Date",
          valueFormatter: (value: Date) =>
            tableFormat === "ascii"
              ? format(value, "dd.MM.yyyy")
              : value.toISOString()
        }
      ],
      ["sumEstimateOfOriginalIssues", "Original Estimate"],
      ["sumEstimateOfAddedIssues", "Added Estimate"],
      ["sumWorklogOfOriginalIssues", "Original Worklog"],
      ["sumWorklogOfAddedIssues", "Added Worklog"]
    ],
    format: tableFormat
  });
}
