import { fromPairs, sum } from "lodash";

import { JiraIssue, JiraIssueFields, JiraSprint } from "@jira-shell/core/jira-client";
import { createHistory } from "../../queries/create-history";

/**
 * Calculates a report, which compares estimations against actual logged work on a daily basis.
 * @see ActualTargetReportRow
 */
export function* calculateActualTargetReport(
  sprint: JiraSprint,
  issues: JiraIssue[]
) {
  // create a day by day history of the issues from their changelogs
  const history = [...createHistory(sprint, issues)];
  // the original version of the issues at sprint start
  const original = history[0];
  // the final version of the issues today or at the end of the sprint
  const final = history[history.length - 1];

  // issues versions of the previous day - used to compare the changes
  let prev = original;

  let sumWorklogOfOriginalIssues = 0;
  let sumWorklogOfAddedIssues = 0;

  for (const current of history.slice(1)) {
    // TODO: this is just an approximation of worklogs
    // if a ticket has worklog on the same day it was added, then no worklog will show up!
    const worklogs = fromPairs(
      current.issues.map(issue => {
        const prevIssue = prev.issues.find(({ key }) => key === issue.key);

        return prevIssue
          ? [issue.key, issue.fields.timespent - prevIssue.fields.timespent]
          : [issue.key, 0];
      })
    );

    // issues that were planned at or before sprint start and remained on the sprint until the end
    const originalIssues = current.issues.filter(
      ({ key }) =>
        original.issues.some(issue => key === issue.key) &&
        final.issues.some(issue => issue.key === key)
    );

    const sumEstimateOfOriginalIssues = sumField(
      originalIssues,
      "timeestimate"
    );

    sumWorklogOfOriginalIssues += sum(
      originalIssues.map(({ key }) => worklogs[key])
    );

    // issues that were added after sprint start and remained until the sprint ended
    const addedIssues = current.issues.filter(
      ({ key }) =>
        !originalIssues.some(issue => issue.key === key) &&
        final.issues.some(issue => issue.key === key)
    );

    const sumEstimateOfAddedIssues = sumField(addedIssues, "timeestimate");

    sumWorklogOfAddedIssues += sum(addedIssues.map(({ key }) => worklogs[key]));

    // any issue that did not remain until the sprint ended
    const removedIssues = current.issues.filter(
      ({ key }) => !final.issues.some(issue => issue.key === key)
    );

    const sumEstimateOfRemovedIssues = sumField(removedIssues, "timeestimate");

    yield {
      date: current.date,
      sumEstimateOfOriginalIssues,
      sumEstimateOfAddedIssues,
      sumEstimateOfRemovedIssues,
      sumWorklogOfOriginalIssues,
      sumWorklogOfAddedIssues
    };

    prev = current;
  }
}

export interface ActualTargetReportRow {
  /**
   * A day between sprint start and end
   */
  date: Date;

  /**
   * The sum of remaining estimate of original issues at `date`
   */
  sumEstimateOfOriginalIssues: number;

  /**
   * The sum of remaining estimate of added issues at `date`
   */
  sumEstimateOfAddedIssues: number;

  /**
   * The sum of remaining estimate of issues removed from the sprint at `date`
   */
  sumEstimateOfRemovedIssues: number;

  /**
   * The sum of logged work on original issues until `date`
   */
  sumWorklogOfOriginalIssues: number;

  /**
   * The sum of logged work on added issues until `date`
   */
  sumWorklogOfAddedIssues: number;
}

// TODO: move to utils
function sumField(issues: JiraIssue[], field: keyof JiraIssueFields) {
  return sum(issues.map(({ fields }) => fields[field]));
}
