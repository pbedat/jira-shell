import chalk from "chalk";
import { sum, values } from "lodash";
import * as yargs from "yargs";

import { WithContextCommandModule } from "../../shared/with-context";
import { createEstimationReport } from "./estimation";

const reports = {
  estimationPerformance: "estimation-performance"
};

export const reportModule: WithContextCommandModule = {
  describe: "Show aggregated reporting data over a list of jira issues",
  command: "report <report>",
  builder: (y: yargs.Argv) => y
    .positional("report", { choices: values(reports) })
    .option("issues", { required: process.stdin.isTTY, type: "array" }),
  handler({ args, context }) {
    switch (args.report) {
      case reports.estimationPerformance:
        return createEstimationReport({ issues: args.issues, project: context.project });
    }
    console.error(`unkown report '${args.report}'`);
  }
};
