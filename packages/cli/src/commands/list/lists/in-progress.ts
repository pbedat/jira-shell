import chalk from "chalk";
import { orderBy } from "lodash";

import * as jira from "@jira-shell/core/jira-client";
import { ApplicationContext } from "../../../shared/with-context";

/**
 * Searches for issues of the provided user, that are in progress.
 * The result will be ordered by 'updated' in descending order.
 * @param args
 */
export async function listInProgressIssues(context: ApplicationContext) {
  const { credentials, url } = context;
  const { username } = credentials;

  const issues = await jira.search({
    jql: `assignee=${username} and statusCategory in ("In Progress") and status != "Resolved"`
  });

  // will print output like:
  // JIRA-1234 a ticket summary (http://your-jira-instance/browser/JIRA-1234)
  process.stdout.write(orderBy(issues, (i) => i.fields.updated, "desc")
    .map((i) => `${chalk.bold(i.key)} ${i.fields.summary} (${chalk.blue(`${url}/browse/${i.key}`)})`)
    .join("\n"));
  process.stdout.write("\n");
}
