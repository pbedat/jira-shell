import chalk from "chalk";
import { a } from "cli-hyperlinks";
import { compact, groupBy, orderBy } from "lodash";

import { formatHours, underline } from "@jira-shell/core/formatting";
import { issueSum, limitWorkload } from "@jira-shell/core/issues";
import * as jira from "@jira-shell/core/jira-client";
import { getPreviousWorkday } from "@jira-shell/core/time-helper";
import { ApplicationContext } from "../../../shared/with-context";

/**
 * lists all issues, that are currently in progress, have been finished the last workday
 * @param args
 */
export async function* listIssuesForDailyMeeting(context: ApplicationContext) {
  const { credentials, url } = context;

  const previousWorkingDay = getPreviousWorkday();

  /**  Uses JQL to collect issues asigned to current user, which
   *  - were resolved on last workingday
   *  - are still in progress
   *  - are upcoming issues in the current sprint
   */
  const jiraResponse: jira.SearchResponse = yield () => jira.searchAsync({
    jql: `(assignee=${credentials.username} AND resolved >= ${previousWorkingDay.toISOString().substring(0, 10)}) OR
       (assignee=${credentials.username} AND statusCategory in ("In Progress") AND status != "Resolved") OR
       (assignee=${credentials.username} AND (status != "Closed" AND statusCategory not in ("In Progress"))) AND
       sprint in openSprints()`
  });

  // group the issues by status
  const issuesByStatus = groupBy(
    await jiraResponse.toPromise(),
    (i: jira.JiraIssue) => i.fields.status.name
  );

  // for each status
  for (const status of ["Resolved", "In Progress", "Open"]) {

    const issues = issuesByStatus[status] || [];

    switch (status) {
      case "Resolved":
        // order resolved issues by resolutiondate asc
        yield renderDailyIssuesByStatus(status, orderBy(issues, ({ fields }) => fields.resolutiondate), url);
        break;
      case "Open":
        // we only want to see the upcoming tickets with a workload
        // fitting into the next 8h and ordered by rank
        const limitedOpenIssues = limitWorkload(issues, 8 * 3600, { orderBy: "rank" });
        yield renderDailyIssuesByStatus("Open (next ~8h)", limitedOpenIssues, url);

        // render stats of the remaining issues
        const remainingIssues = issues.length - limitedOpenIssues.length;
        const remainingSum = issueSum(issues, "timeestimate") - issueSum(limitedOpenIssues, "timeestimate");
        yield chalk.italic.gray(`>> ${remainingIssues} issues to go: ${formatHours(remainingSum)}`);
        break;
      case "In Progress":
        // order in progress issues by their rank
        yield renderDailyIssuesByStatus(status, orderBy(issues, ({ fields }) => fields.rank), url);
        break;
    }
    yield "\n";
  }
}

/**
 * Renders a status and issues belonging to it
 */
function* renderDailyIssuesByStatus(status: string, issues: jira.JiraIssue[], url: string) {
  // render the status, nicely underlined of course
  yield `${underline(chalk.blue(status))}\n`;

  if (!issues || !issues.length) {
    return chalk.bold.yellow(`>> no issues '${status}'\n`);
  }

  // and then all issues with this status
  for (const issue of issues) {
    yield `${renderDailyIssue({ issue, url })}\n`;
  }
}

interface DailyTemplateData {
  issue: jira.JiraIssue;
  url: string;
}
/**
 * Renders a Jira Issue and Project to follwing output:
 * "bold(Foo-1337) This could be your Ticket summary 4h (blue(https://foo-jira/Foo-1337))"
 * <Issue Key> <Summary> [<time remaining>] <Link>
 * @param data JiraIssue and JiraShellProject object
 */
export function renderDailyIssue(data: DailyTemplateData): string {
  const { issue, url } = data;
  const { key, fields } = issue;

  return compact([
    chalk.gray(key),
    chalk.bold(fields.summary),
    !fields.resolutiondate ? chalk.green(`${fields.timeestimate / 3600}h`) : "",
    `(${a(`${url}/browse/${key}`, "Open in Jira")})`
  ]).join(" ");
}
