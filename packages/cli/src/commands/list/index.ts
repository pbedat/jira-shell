import { values } from "lodash";
import * as yargs from "yargs";

import { WithContextCommandModule } from "../../shared/with-context";
import { listInProgressIssues, listIssuesForDailyMeeting, listSprints } from "./lists";

const listTypes = {
  inProgress: "in-progress",
  sprints: "sprints",
  daily: "daily"
};

export const listModule: WithContextCommandModule = {
  describe: "lists jira issues",
  command: "list <list>",
  aliases: "ls",
  builder: (y: yargs.Argv) =>
    y.positional("list", { choices: values(listTypes) }),
  handler({args, context}) {
    switch (args.list) {
      case listTypes.inProgress:
        listInProgressIssues(context);
        break;
      case listTypes.sprints:
        listSprints();
        break;
      case listTypes.daily:
        return listIssuesForDailyMeeting(context);
    }
  }
};
