import chalk from "chalk";
import { addSeconds, differenceInSeconds, format, startOfDay } from "date-fns";
import { chain, last, max, range, repeat, sum } from "lodash";
import { table } from "table";

import { underline } from "@jira-shell/core/formatting";
import { getWorklog, JiraIssue, JiraWorklog, search } from "@jira-shell/core/jira-client";
import { WithContextCommandModule } from "../../shared/with-context";

// should be configurable:
const WORKING_HOURS = 8;

/**
 * @see the command description
 */
export const timesheetModule: WithContextCommandModule = {
  command: "timesheet",
  describe: "prints your logged work as a timesheet",
  builder: argv => argv.option("date", { default: new Date().toISOString() }),
  async *handler({ args, context }) {
    const date = new Date(args.date);
    const user = context.credentials.username;

    yield underline(`Worklog ${format(args.date, "dd.MM.yy")}`, "=");
    yield "\n\n";

    // fetch all issues where the `user` has logged some work for the given `date`
    const issues: JiraIssue[] = yield () => search({
      jql: `worklogDate = ${format(startOfDay(args.date), "yyyy-MM-dd")} AND worklogAuthor = ${user}`
    });

    // extract all the worklog entris of those issues
    const worklogs = chain(await Promise.all(issues.map(getWorklog)))
      .flatten()
      // ...and filter for entries logged by the current `user` for the given `date`
      .filter(filterWorklog({ user, date }))
      // ...and oder by the worlog date
      .orderBy(w => new Date(w.started).getTime())
      .valueOf();

    if (worklogs.length === 0) {
      return ">> No worklog entries found <<\n";
    }

    // get the sum of the worked seconds
    const sumWorklog = sum(worklogs.map(({ timeSpentSeconds }) => timeSpentSeconds));

    // scales the width of the following progress bar
    const PROGRESS_SCALING_FACTOR = 4;
    yield [
      // summary of the worked time e.g.: 5.5h / 8h
      `${sumWorklog / 3600}h / 8h `,
      // prints a progress bar, which shows how much of the targeted WORKING_HOURS have already been logged
      // e.g. 4h / 8h => [#################__________________]
      // A progress unit is 1h * PROGRESS_SCALING_FACTOR
      `[${
      range(0, WORKING_HOURS * PROGRESS_SCALING_FACTOR)
        .map(i => (i * 3600 / PROGRESS_SCALING_FACTOR) < sumWorklog ? "#" : "_")
        .join("")
      }]\n\n`
    ].join("");

    yield table(
      [
        [ "Start", "Time", "Issue", "Worklog" ].map(str => chalk.bold(str)),
        ...renderSheetTable(issues, worklogs)
      ],
      {
        columns: {
          0: { alignment: "center", width: 8 },
          1: { alignment: "right" }
        }
      });
  }
};

/**
 * Returns a filter predicate that returns true
 * when a jira worklog matches the provided user and date
 * @param filter
 */
function filterWorklog(filter: { user: string, date: Date }) {
  return (worklog: JiraWorklog) => {
    return worklog.author.name === filter.user
      && startOfDay(worklog.started).getTime() === startOfDay(filter.date).getTime();
  };
}

/**
 * Renders the worklog entries as tabular data:
 * [ startTime, duration, issue key, issue summary, worklog comment ]
 */
function* renderSheetTable(issues: JiraIssue[], worklogs: JiraWorklog[]) {

  const sumWorklog = worklogs.reduce((s, w) => w.timeSpentSeconds + s, 0);

  // tslint:disable-next-line:prefer-for-of
  for (let i = 0; i < worklogs.length; i++) {
    const worklog = worklogs[i];
    const issue = issues.find(({ id }) => id === worklog.issueId);

    if (!issue) {
      continue;
    }

    // render a table row:
    // [ time, duration, issue key, issue summary, comment]
    yield [
      chalk.bold(format(worklog.started, "HH:mm")),
      worklog.timeSpent,
      issueLabel(issue),
      // `table` does no allow control characters - remove them:
      encodeURIComponent(worklog.comment as string).replace(/\%\w\w/g, " ")
    ];

    // is there a gap between worklogs?
    if (worklog !== last(worklogs) && ended(worklog) < new Date(worklogs[i + 1].started)) {

      // how long is the gap?
      const gapSeconds = differenceInSeconds(worklogs[i + 1].started, worklog.started) - worklog.timeSpentSeconds;

      // print a 'template entry' for the gap
      yield templateEntry(issues, ended(worklog), gapSeconds);
    }
  }

  // if the sum of the worklog is < WORING_HOURS
  if (sumWorklog < (8 * 3600) && worklogs.length > 0) {
    const lastWorklog = last(worklogs) || {} as any;
    // print another 'template entry' after the last worklog to fill up the remaining todo working time
    yield templateEntry(
      issues,
      ended(lastWorklog),
      (WORKING_HOURS * 3600 - sumWorklog)
    );
  }
}

/**
 * Prints a template entry of the timesheet to fill gaps.
 * Example:
 * ( 13:00 ) | 1.5 h | ------- | ---------------------- | ¯\_(ツ)_/¯
 */
function templateEntry(issues: JiraIssue[], start: string | Date, durationSeconds: number) {
  return [
    `(${format(start, "HH:mm")})`,
    `(${durationSeconds / 3600} h)`,
    repeat("-", max(issues.map(issue => issueLabel(issue).length))),
    "¯\\_(ツ)_/¯"
  ].map(str => chalk.gray(str));
}

function ended(worklog: JiraWorklog) {
  return addSeconds(worklog.started, worklog.timeSpentSeconds);
}

function issueLabel({key, fields}: JiraIssue) {
  return `${key} - ${fields.summary}`;
}
