import chalk from "chalk";
import { sortBy, sum, uniq } from "lodash";
import { table } from "table";

import { formatHours } from "@jira-shell/core/formatting";
import * as jira from "@jira-shell/core/jira-client";
import { UNASSIGNED } from "../../constants";
import { ApplicationContext } from "../../shared/with-context";
import * as selectors from "./selectors";

interface WorklogArgs {
  quiet?: boolean;
  sprintName?: jira.SprintNameArg;
  groupBy?: keyof jira.JiraIssueFields;
}

/**
 * Renders a table, listing
 *  - all assignees,
 *  - their issue count
 *  - and their total remaining estimate
 *
 * When you do not specify a sprintName explicitly the workload will be calculated for
 *  - the next upcoming sprint by default
 *  - the current running sprint if there is no upcoming sprint planned
 * @param context
 */
export async function calculateWorkload(args: WorklogArgs, context: ApplicationContext) {
  const { project } = context;
  const { sprintName, quiet, groupBy } = args;
  // select a sprint based on the provided sprintName
  const sprint = jira.getSprint(args.sprintName, await jira.getAllSprints());

  if (!sprint) {
    process.stderr.write("no workload data available: there is no upcoming sprint at the moment");
    return;
  }

  if (process.stdout.isTTY && !quiet) {
    process.stdout.write(chalk.bold(`\nWorkload for sprint >>${chalk.blue(sprint.name)}<<\n`));
  }

  // get all issues from that sprint
  const issues = await jira.search({
    jql: [
      `sprint = ${sprint.id}`,
      `AND project = ${project}`
    ].join(" ")
  });

  if (!quiet && !process.stdout.isTTY) {
    process.stdout.write(JSON.stringify(issues));
    process.exit(0);
  }

  if (args.quiet) {
    process.stdout.write(issues.map(({ key }) => key).join("\n"));
    return;
  }
  // render the workload as a table
  process.stdout.write(
    renderWorkloadTable(selectors.getWorkload(issues), groupBy)
  );
}

/**
 * renders the provided workload as a table:
 * | <assignee display name> | <issue count> | <remaining estimate> h |
 * | ...                     | ...           | ...                    |
 * | Unassigned              | <issue conut> | <remaining estimate> h |
 * | Total                   | <issue count> | <remaining estimate> h |
 */
function renderWorkloadTable(workload: selectors.SprintWorkload, groupBy?: keyof jira.JiraIssueFields) {
  const groupByPredicate = createGroupByPredicate(groupBy);
  let groups = [] as string[];
  if (groupByPredicate) {
    groups = uniq(workload.issues.map(groupByPredicate));
  }

  // finally render the workload as a table
  return table([
    // ...with a bold table header; adds groups to header when "groupBy" is supplied
    (groups.length > 0 ? ["Assignee", "Issues", ...groups, "Workload"].map((h) => chalk.bold(h)) :
      ["Assignee", "Issues", "Workload"].map((h) => chalk.bold(h))),
    // sorted by the assignee display names
    ...sortBy(
      // with the assignees display name and their total remaining estimate in hours, as workload
      // mapping the workload to a two dimensional table data array:
      // [ [ <assignee display name>, <total estimate hours> ], ... ]
      workload.assigneeWorkloads,
      // make sure that unassigned is at the bottom of the table
      (w) => w.assignee === UNASSIGNED ? undefined : w.assignee.displayName
    ).map((w) => ([
      w.assignee.displayName,
      w.issues.length,
      ...getGroupRow(groups, w.issues, groupByPredicate),
      formatHours(w.totalRemainingEstimate)
    ].map(
      // the unassinged row should be painted gray
      (str) => w.assignee === UNASSIGNED ? (chalk.italic.gray(str.toString())) : str))
    ),
    // and a bold footer showing the totals,
    [
      "Total",
      workload.issues.length,
      ...getGroupRow(groups, workload.issues, groupByPredicate),
      formatHours(workload.totalRemainingEstimate)
    ].map((s) => chalk.bold(s.toString()))
  ],
    { columns: { 1: { alignment: "right" }, 2: { alignment: "right" } } });
}

function* getGroupRow(groups: string[], issues: jira.JiraIssue[], groupByPredicate?: (issue: jira.JiraIssue) => string) {
  for (const group of groups) {
    if (groupByPredicate) {
      const groupIssues = issues.filter(issue => groupByPredicate(issue) === group);
      yield `(${groupIssues.length}) ${formatHours(sum(groupIssues.map(({ fields }) => fields.timeestimate)))}`;
    }
  }
}

function createGroupByPredicate(group?: keyof jira.JiraIssueFields) {
  switch (group) {
    case "status":
      return (issue: jira.JiraIssue) => issue.fields.status.name;
    case "priority":
      return (issue: jira.JiraIssue) => issue.fields.priority.name;
    default:
      if (group) {
        console.log(`--groupBy="${group}" is not supported. Try "status" or "priority" instead`);
      }
      break;
  }
}
