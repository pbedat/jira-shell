import * as yargs from "yargs";

import { WithContextCommandModule } from "../../shared/with-context";
import { calculateWorkload } from "./workload";

const description =
`Calculates the workload explicitly for this sprint.
When no sprint is provided the workload will be calculated for:
 - the next upcoming sprint (Default)
 - the currently running sprint, when no upcoming sprint is planned
Both, upcoming and currently running sprints, are available with funcion variables:
 - __current: the current sprint
 - __next: the next upcomping sprint
`;

export const workloadModule: WithContextCommandModule = {
  describe: "calculates the workload per assignee",
  command: "workload [--sprint]",
  builder: (y: yargs.Argv) =>
    y.option("sprint", { description })
    .option("quiet", { alias: "q", description: "print only issue keys"})
    .option("groupBy", { description: "Groups Issues by the provided issue field"}),
  handler({args, context}) {
    calculateWorkload({...args, sprintName: args.sprint}, context);
  }
};
