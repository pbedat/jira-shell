import { groupBy, map, memoize, sum } from "lodash";

import * as jira from "@jira-shell/core/jira-client";
import { UNASSIGNED } from "../../../constants";

export function getWorkload(issues: jira.JiraIssue[]): SprintWorkload {
  // calculate the assignee workloads...
  const assigneeWorkloads = map(
    // for each assignee...
    groupBy(issues, ({ fields }) => fields.assignee ? fields.assignee.name : UNASSIGNED.name),
    // create an assignee workload entry
    (assigneeIssues, assigneeName) => ({
      // ...with the assignee
      assignee: findAssignee(assigneeName, issues),
      // ...its assigned issues
      issues: assigneeIssues,
      // ...and the total remaining estimate on those issues
      totalRemainingEstimate: sum(assigneeIssues.map(({ fields }) => fields.timeestimate))
    }));

  // render the workload as a table
  return {
    issues,
    assigneeWorkloads,
    // sum up all timestimates of all issues
    totalRemainingEstimate: sum(assigneeWorkloads.map(a => a.totalRemainingEstimate))
  };
}

export interface SprintWorkload {
  issues: jira.JiraIssue[];
  totalRemainingEstimate: number;
  assigneeWorkloads: AssigneeWorkload[];
}

interface AssigneeWorkload {
  assignee: jira.JiraAssignee;
  issues: jira.JiraIssue[];
  totalRemainingEstimate: number;
}

interface WorkloadProps {
  issues: jira.JiraIssue[];
  groupBy?: keyof jira.JiraIssueFields;
}

/**
 * find an assignee with the given `name` among some issues
 * throws an error when the assignee cannot be found
 */
const findAssignee = memoize((name: string, issues: jira.JiraIssue[]) => {
  // undefined names occur, when issues have no assignee
  // in this case we return a virtual `Unassigned` JiraAsignee
  if (!name) {
    return UNASSIGNED;
  }

  // find an issues with the assignee
  const issueWithAssignee = issues.find(({ fields }) => fields.assignee && fields.assignee.name === name);

  if (!issueWithAssignee) {
    throw new Error(`unable to find a issue with the assignee '${name}'`);
  }

  return issueWithAssignee.fields.assignee;
});
