This project ist maintained with `lerna`: https://github.com/lerna/lerna

Packages:
 - [cli](https://gitlab.com/pbedat/jira-shell/tree/master/packages/cli): the jira-shell console app
 - [core](https://gitlab.com/pbedat/jira-shell/tree/master/packages/core): the jira-shell core lib